ARG BASE_IMG

FROM ${BASE_IMG}
LABEL org.opencontainers.image.authors="texnoman@itmo.com"

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive

ENV GAZEBO_GUI true
ENV RVIZ_GUI true

RUN cd src && git clone -b noetic-devel https://github.com/RobotnikAutomation/rbvogui_sim.git\
    && git clone https://github.com/RobotnikAutomation/robotnik_sensors.git\
    && git clone https://github.com/RobotnikAutomation/joint_read_command_controller.git\
    && git clone https://github.com/RobotnikAutomation/robotnik_pad.git\
    && git clone -b noetic-devel https://github.com/RobotnikAutomation/rbvogui_common.git\
    && git clone https://github.com/kirillin/simple_gripper_gazebo.git\
    && git clone https://github.com/JenniferBuehler/gazebo-pkgs.git\
    && git clone https://github.com/roboticsgroup/roboticsgroup_upatras_gazebo_plugins.git\
    && git clone https://github.com/JenniferBuehler/general-message-pkgs.git

RUN cd src && git clone https://github.com/RobotnikAutomation/teleop_panel.git

RUN rm -rf /ros_ws/src/rbvogui_common/rbvogui_pad

RUN cd src/rbvogui_common/libraries && apt install -y ./* && apt --fix-broken install -y ./*

RUN ROS_DISTRO=noetic || rosdep install --from-paths src --ignore-src -y

RUN catkin build

COPY . src/master_demo_scene

RUN cd src/master_demo_scene && catkin build --this

HEALTHCHECK --interval=20s --timeout=1s --retries=3 --start-period=20s CMD if [[ $(rostopic list 2> /dev/null | wc -c) > 0 ]]; then exit 0; fi;
CMD ["/bin/bash", "-ci", "roslaunch master_demo_scene start_scene.launch gazebo_gui:=${GAZEBO_GUI} rviz_gui:=${RVIZ_GUI}"]