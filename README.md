![scene pic](docs/figures/scene_view.png)

# demo_master_scene
Репозиторий со сценой для demo версии олимпиады магистров.
Для локальной сборки достаточно выполнить, не забыв выполнить при этом `docker login` в gitlab.

```bash
docker login registry.gitlab.com -u <username> -p <token>
```

```bash
docker build --pull -t scene-master-demo-img . --build-arg  BASE_IMG=registry.gitlab.com/beerlab/iprofi2023/demo/master/base:nvidia-latest
```

Для дальнейшего запуска дадим возможность авторизации для X-server извне:
```bash
xhost +local:docker
```

Далее для запуска достаточно склонировать репозиторий, предоставляемый участникам https://gitlab.com/beerlab/iprofi2023/demo/master и запустить:
```bash
docker compose -f docker-compose.nvidia.local.yml up --build --pull always
```
Или без docker compose:
```bash
docker run -ti scene-master-demo-img
```

Открыть новую bash-сессию в контейнере

```bash
docker exec -ti scene-master-demo-img bash
```

## License
MIT License
